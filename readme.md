# Super Jab

**Slide Show**

Super Jab is a basic, text based, slide show application. It uses the `cmd.Cmd`
fraemwork for designing and building slide shows and the `colorama` library to
facilitate the colorful display of slide shows. It even provides various
transition effects like fade in and window blinds.

