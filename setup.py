from setuptools import setup


setup(
      name="Super Jab",
      version="0.1.0",
      packages=["superjab"],
      url="https://bitbucket.org/protocypher/ma-superjab",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A slide show creation and presentation application."
)

